<?php

use App\Models\Participant;
use App\Models\Vacation;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->id();
            $table->text("name");
            $table->foreignIdFor(Participant::class)->nullable()->constrained()->nullOnDelete()->cascadeOnUpdate();
            $table->text("link");
            $table->text("accomodation_price_person");
            $table->text("travel_cost_person");
            $table->text("address");
            $table->foreignIdFor(Vacation::class)->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->boolean("is_final")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
};
