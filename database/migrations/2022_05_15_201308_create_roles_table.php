<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->text("name");
            $table->boolean("organiser")->default(false);
            $table->boolean("suggest_place")->default(false);
            $table->boolean("change_status")->default(false);
            $table->boolean("invite")->default(false);
            $table->boolean("vote")->default(false);
            $table->boolean("comment")->default(false);
            $table->boolean("create_poll")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
};
