<?php

use App\Models\Participant;
use App\Models\Vacation;
use App\Models\VacationComment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacation_comments', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Participant::class)->nullable()->constrained()->nullOnDelete()->cascadeOnUpdate();
            $table->foreignIdFor(Vacation::class)->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->longText("comment");
            $table->foreignIdFor(VacationComment::class)->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacation_comments');
    }
};
