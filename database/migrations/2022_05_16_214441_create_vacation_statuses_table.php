<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacation_statuses', function (Blueprint $table) {
            $table->id();
            $table->text("name");
            $table->boolean("under_organization")->default(true);
            $table->boolean("time_selection")->default(true);
            $table->boolean("collecting_places")->default(false);
            $table->boolean("choosing_place")->default(false);
            $table->boolean("booking")->default(false);
            $table->boolean("retained")->default(false);
            $table->boolean("cancaled")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacation_statuses');
    }
};
