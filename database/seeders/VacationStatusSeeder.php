<?php

namespace Database\Seeders;

use App\Models\VacationStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VacationStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VacationStatus::insert([
            [
                "name"=>"Időpont kiválasztása",
                "under_organization"=>1,
                "time_selection"=>1,
                "collecting_places"=>0,
                "choosing_place"=>0,
                "booking"=>0,
                "retained"=>0,
                "cancaled"=>0,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                "name"=>"Lehetséges uticélok összegyűjtése",
                "under_organization"=>1,
                "time_selection"=>0,
                "collecting_places"=>1,
                "choosing_place"=>0,
                "booking"=>0,
                "retained"=>0,
                "cancaled"=>0,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                "name"=>"Uticél kiválasztása",
                "under_organization"=>1,
                "time_selection"=>0,
                "collecting_places"=>0,
                "choosing_place"=>1,
                "booking"=>0,
                "retained"=>0,
                "cancaled"=>0,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                "name"=>"Lefoglalás alatt",
                "under_organization"=>1,
                "time_selection"=>0,
                "collecting_places"=>0,
                "choosing_place"=>0,
                "booking"=>1,
                "retained"=>0,
                "cancaled"=>0,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                "name"=>"Megtartott nyaralás",
                "under_organization"=>0,
                "time_selection"=>0,
                "collecting_places"=>0,
                "choosing_place"=>0,
                "booking"=>0,
                "retained"=>1,
                "cancaled"=>0,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                "name"=>"Lemondott nyaralás",
                "under_organization"=>1,
                "time_selection"=>0,
                "collecting_places"=>0,
                "choosing_place"=>0,
                "booking"=>0,
                "retained"=>0,
                "cancaled"=>1,
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ]
        ],);
    }
}
