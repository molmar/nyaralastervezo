<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [
                'name'=>'Szervező',
                'organiser'=>1,
                'suggest_place'=>0,
                'change_status'=>0,
                'invite'=>0,
                'vote'=>0,
                'comment'=>0,
                'create_poll'=>0,
                'description'=>'',
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                'name'=>'Résztvevő',
                'organiser'=>0,
                'suggest_place'=>1,
                'change_status'=>0,
                'invite'=>1,
                'vote'=>1,
                'comment'=>1,
                'create_poll'=>1,
                'description'=>'',
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                'name'=>'Vendég',
                'organiser'=>0,
                'suggest_place'=>0,
                'change_status'=>0,
                'invite'=>0,
                'vote'=>0,
                'comment'=>1,
                'create_poll'=>0,
                'description'=>'',
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ],[
                'name'=>'Szemlélő',
                'organiser'=>0,
                'suggest_place'=>0,
                'change_status'=>0,
                'invite'=>0,
                'vote'=>0,
                'comment'=>0,
                'create_poll'=>0,
                'description'=>'',
                "created_at"=>date("Y-m-d H:i:s"),
                "updated_at"=>date("Y-m-d H:i:s"),
            ]
        ],);
    }
}
