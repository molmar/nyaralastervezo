<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacationStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "under_organization",
        "time_selection",
        "collecting_places",
        "choosing_place",
        "booking",
        "retained",
        "cancaled",
    ];
}
