<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        "participant_id",
        "place_id",
        "url",
    ];

    public function participant(){
        return $this->belongsTo(Participant::class);
    }
}
