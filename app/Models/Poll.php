<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    use HasFactory;

    protected $fillable = [
        "question",
        "participant_id",
        "vacation_id",
    ];

    public function participant(){
        return $this->belongsTo(Participant::class);
    }

    public function vacation(){
        return $this->belongsTo(Vacation::class);
    }

    public function options(){
        return $this->hasMany(PollOption::class);
    }
}
