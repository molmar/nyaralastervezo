<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "description",
        "participant_id",
        "link",
        "accomodation_price_person",
        "travel_cost_person",
        "address",
        "vacation_id",
        "is_final",
    ];

    public function participant(){
        return $this->belongsTo(Participant::class);
    }

    public function vacation(){
        return $this->belongsTo(Vacation::class);
    }

    public function comments(){
        return $this->hasMany(PlaceComment::class);
    }

    public function votes(){
        return $this->hasMany(PlaceVote::class);
    }

    public function images(){
        return $this->hasMany(Image::class);
    }
}
