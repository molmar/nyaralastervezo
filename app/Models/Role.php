<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "organiser",
        "suggest_place",
        "change_status",
        "invite",
        "vote",
        "comment",
        "create_poll",
    ];
}
