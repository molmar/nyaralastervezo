<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "vacation_id",
        "role_id",
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function vacation(){
        return $this->belongsTo(Vacation::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }
}
