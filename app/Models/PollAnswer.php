<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PollAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
        "poll_option_id",
        "participant_id",
    ];

    public function participant(){
        return $this->belongsTo(Participant::class);
    }

    public function pollOption(){
        return $this->belongsTo(PollOption::class,"poll_option_id");
    }
}
