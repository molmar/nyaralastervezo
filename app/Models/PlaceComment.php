<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaceComment extends Model
{
    use HasFactory;

    protected $fillable = [
        "participant_id",
        "place_id",
        "comment",
        "place_comment_id",
    ];

    public function participant(){
        return $this->belongsTo(Participant::class);
    }

    public function place(){
        return $this->belongsTo(Place::class);
    }

    public function parent(){
        return $this->belongsTo(PlaceComment::class);
    }
}
