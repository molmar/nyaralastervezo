<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacation extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "nick",
        "user_id",
        "thumbnail_id",
        "from",
        "to",
        "organiser_link",
        "vacation_status_id",
    ];

    public function participants(){
        return $this->hasMany(Participant::class);
    }

    public function creator(){
        return $this->belongsTo(User::class,"user_id","id");
    }

    public function thumbnail(){
        return $this->belongsTo(Thumbnail::class);
    }

    public function places(){
        return $this->hasMany(Place::class);
    }

    public function comments(){
        return $this->hasMany(VacationComment::class);
    }

    public function polls(){
        return $this->hasMany(Poll::class);
    }

    public function status(){
        return $this->belongsTo(VacationStatus::class,"vacation_status_id","id");
    }
}
