<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VacationComment extends Model
{
    use HasFactory;

    protected $fillable = [
        "participant_id",
        "vacation_id",
        "comment",
        "vacation_comment_id",
    ];

    public function participant(){
        return $this->belongsTo(Participant::class);
    }

    public function vacation(){
        return $this->belongsTo(Vacation::class);
    }

    public function parent(){
        return $this->belongsTo(VacationComment::class);
    }
}
