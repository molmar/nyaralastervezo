<?php

namespace App\Http\Controllers;

use App\Models\Participant;
use App\Models\Role;
use App\Models\Vacation;
use App\Models\VacationStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class VacationController extends Controller
{
    public function create(Request $request){
        $request->validate([
            "vakacioNeve" => ["required","string"],
            "vakacioKezdes" => ["required_unless:idopontKivalasztva,null","string","nullable"],
            "vakacioVege" => ["required_unless:idopontKivalasztva,null","string","nullable"],
            "vakacioIndexkepId" => ["required","numeric","integer","exists:thumbnails,id"],
        ]);

        $newVacation = new Vacation;

        $newVacation->name = $request->vakacioNeve;
        $newVacation->nick = Str::random(6);
        $newVacation->user_id = Auth::user()->id;
        $newVacation->thumbnail_id = $request->vakacioIndexkepId;
        if (!empty( $request->idopontKivalasztva )) {
            $newVacation->from = $request->vakacioKezdes;
            $newVacation->to = $request->vakacioVege;
            $newVacation->organiser_link = $request->vakacioIdopontszervezoLink;
            $vacationStatus = VacationStatus::where("collecting_places",1)->first();
            $newVacation->vacation_status_id = $vacationStatus->id;
        }else{
            $vacationStatus = VacationStatus::where("time_selection",1)->first();
            $newVacation->vacation_status_id = $vacationStatus->id;
        }

        if($newVacation->save()){
            $role = Role::where('organiser',1)->first();

            Participant::create([
                "user_id" => Auth::user()->id,
                "vacation_id" => $newVacation->id,
                "role_id" => $role->id,
            ]);

            return redirect(url("nyaralasok/".$newVacation->nick));
        }

    }

    public function show($nick){
        $vacation = Vacation::where("nick",$nick)->first();
        $roles = Role::all();
        $curUsr = $vacation->participants()->where("user_id",Auth::id())->first();

        if(empty($vacation->participants()->where("user_id",Auth::id())->first())){
            return redirect('attekintes')->with('error',"Nincs jogosultsága a nyaralás megtekintéséhez!");
        }

        $data = [
            "Vacation" => $vacation,
            "Roles" => $roles,
            "curUser" => $curUsr,
        ];

        return view("viewvacation",$data);
    }

    public function invite($nick){
        $vacation = Vacation::where("nick",$nick)->first();
        $prt = Participant::where('vacation_id',$vacation->id)
                        ->where("user_id",Auth::id())
                        ->first();

        $rRole = Role::where("name","Résztvevő")->first();

        if(empty($prt)){
            $newPrt = new Participant;

            $newPrt->user_id = Auth::id();
            $newPrt->vacation_id = $vacation->id;
            $newPrt->role_id = $rRole->id;

            $newPrt->save();
        }

        return redirect('nyaralasok/'.$nick);
    }
}
