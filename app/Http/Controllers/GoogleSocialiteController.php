<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Socialite;

class GoogleSocialiteController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }
       
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleCallback()
    {
        try {
     
            $user = Socialite::driver('google')->user();
      
            $finduser = User::where('social_id', $user->id)->first();
      
            if($finduser){

                $finduser->name = $user->name;
                $finduser->picture = $user->avatar;

                if($finduser->isDirty()){
                    $finduser->save();
                }
      
                Auth::login($finduser);
      
            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'social_id'=> $user->id,
                    'social_type'=> 'google',
                    'password' => Hash::make(Str::random(40)),
                    'picture' => $user->avatar,
                ]);
     
                Auth::login($newUser);
            }

            if(session("redirect")){
                return redirect(session("redirect"));
            }else{
                return redirect("attekintes");
            }
     
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
