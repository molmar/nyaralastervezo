<?php

namespace App\Http\Controllers;

use App\Models\Thumbnail;
use App\Models\User;
use App\Models\Vacation;
use App\Models\VacationStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function dashboard(){
        $uOStatuses = VacationStatus::where("under_organization",1)->get("id")->toArray();
        $oUVacations = User::find(Auth::id())->vacations()->whereIn("vacation_status_id",$uOStatuses)->get();
        
        $oStatuses = VacationStatus::where("retained",1)->get("id")->toArray();
        $oVacations = User::find(Auth::id())->vacations()->whereIn("vacation_status_id",$oStatuses)->get();
        
        $cStatuses = VacationStatus::where("cancaled",1)->get("id")->toArray();
        $cVacations = User::find(Auth::id())->vacations()->whereIn("vacation_status_id",$cStatuses)->get();

        $data = [
            "OUVacations" => $oUVacations,
            "OVacations" => $oVacations,
            "CVacations" => $cVacations,
        ];

        return view("dashboard",$data);
    }

    public function newVacation(){

        $thumbnails = Thumbnail::all();

        $data = [
            "Thumbnails" => $thumbnails,
        ];

        return view("newvacation",$data);
    }
}
