<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Models\Place;
use App\Models\Vacation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlaceController extends Controller
{
    public function saveNew(Request $request, $nick){

        $request->validate([
            "uticelMegnevezese"=>["required","string"],
            "uticelCim"=>["required","string"],
            "uticelLink"=>["nullable","string"],
            "szemelyenkentiSzallasAr"=>["required","numeric","integer","min:0"],
            "szemelyenkentiUtazasiKoltseg"=>["required","numeric","integer","min:0"],
            "uticelKepek"=>["required"],
            "uticelLeiras"=>["required","string"],
        ]);

        $vacation = Vacation::where("nick",$nick)->first();

        if($vacation==null){
            return redirect()->back()->with("error","A kiválasztott vakáció nem létezik!");
        }

        $place = new Place();

        $place->name = $request->uticelMegnevezese;
        $place->description = $request->uticelLeiras;
        $place->participant_id = Auth::user()->id;
        $place->link = $request->uticelLink;
        $place->accomodation_price_person = $request->szemelyenkentiSzallasAr;
        $place->travel_cost_person = $request->szemelyenkentiUtazasiKoltseg;
        $place->address = $request->uticelCim;
        $place->vacation_id = $vacation->id;
        $place->is_final = 0;

        if($place->save()){

            foreach($request->file("uticelKepek") as $Kep){
                $image = new Image();
                $image->participant_id = Auth::user()->id;
                $image->place_id = $place->id;
                $image->url = $Kep->store("uticel_kepek",['disk'=>"public"]);

                $image->save();
            }

            return redirect()->back()->with("success", "Úti cél sikeresen létrehozva!");
            

        }else{
            return redirect()->back()->with("error","Ismeretlen hiba lépett fel az úti cél mentése közben.");
        }



    }
}
