<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function show(Request $request){
        
        if($request->input("redirect")){
            $request->session()->put("redirect",$request->input("redirect"));
        }else{
            $request->session()->forget("redirect");
        }

        if(Auth::check()){
            return redirect('attekintes');
        }

        return view('login');
    }
}
