<?php
    use Illuminate\Support\Facades\Storage;
?>
@extends('layouts.app')

@section('content')
    <h3 class="fw-bold">
        {{ $Vacation->name }}
    </h3>
    <div class="row justify-content-around gy-4">
        <div class="col-12 col-md-4">
            <div class="row gy-4">
                <div class="col-12 shadow-lg rounded p-3">
                    <h5 class="fw-bold">Áttekintés</h5>
                    <hr class="mb-3 mt-1">
                    <ul class="list-unstyled fw-bold">
                        <li>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa-solid fa-calendar-days" data-bs-toggle="tooltip" title="Nyaralás időpontja"></i>
                                </div>
                                <div class="col-9">
                                    @if (!empty($Vacation->from) && !empty($Vacation->to))
                                        {{ $Vacation->from }} - {{ $Vacation->to }}
                                    @else
                                        <i>Még nincs kiválasztott időpont</i>
                                    @endif
                                </div>
                            </div>
                        </li>
                        @if (!empty($Vacation->organiser_link))
                            <li>
                                <div class="row">
                                    <div class="col-1">
                                        <i class="fa-solid fa-link" data-bs-toggle="tooltip" title="Időpontszervező URL"></i>
                                    </div>
                                    <div class="col-9">
                                        <a href="{{ $Vacation->organiser_link }}" target="_blank" rel="noopener noreferrer" class="text-dark">Időpontszervező URL</a>
                                    </div>
                                </div>
                            </li>
                        @endif
                        <li>
                            <div class="row">
                                <div class="col-1">
                                    <i class="fa-solid fa-user-astronaut" data-bs-toggle="tooltip" title="Nyaralást létrehozó"></i>
                                </div>
                                <div class="col-9">
                                    {{ $Vacation->creator->name }}
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-12 shadow-lg rounded p-3">
                    <div class="row">
                        <div class="col-10 text-start">
                            <h5 class="fw-bold">Meghívott személyek</h5>
                        </div>
                        <div class="col-2 text-end">
                            <i class="fa-solid fa-user-plus cursor-pointer" data-bs-toggle="modal" data-bs-target="#newParticipantModal"></i>
                        </div>
                    </div>
                    <hr class="mb-3 mt-1">
                    <ul class="list-unstyled">
                        @foreach ($Vacation->participants as $Participant)
                            <li class="mb-3">
                                <div class="row">
                                    <div class="col-10">
                                            
                                        <div class="d-flex align-items-center">
                                            <img src="{{ $Participant->user->picture }}" class="profile-img" alt="">
                                            <div class="d-block lh-1 ms-2">
                                                <span class="fw-bold h5 mb-0">
                                                    {{$Participant->user->name}}
                                                </span><br>
                                                <span class="text-muted">
                                                    {{$Participant->role->name}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2 text-end">
                                        @if ($Participant->user->id != $Vacation->creator->id && $curUser->role->organiser == 1)
                                            <i class="fa-solid fa-user-gear cursor-pointer"></i>
                                        @endif
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-7">
            <div class="row gy-4">
                <div class="col-12 shadow-lg rounded p-3">
                    <div class="row">
                        <div class="col-10 text-start">
                            <h5 class="fw-bold">Úti cél ajánlások</h5>
                        </div>
                        <div class="col-2 text-end">
                            <i class="fa-solid fa-map-location-dot cursor-pointer" data-bs-toggle="modal" data-bs-target="#newPlaceModal"></i>
                        </div>
                    </div>
                    @if (count($Vacation->places) > 0)
                        <div id="uticelokKorhinta" class="carousel carousel-dark slide w-100" data-bs-ride="carousel">
                            <div class="carousel-inner">

                                <?php
                                    $first = true;
                                    $item = 1;
                                ?>

                                @foreach ($Vacation->places as $Place)

                                    @if ($first == true)
                                        <div class="carousel-item active">
                                            <div class="row justify-content-center">
                                            <?php
                                                $first = false;
                                            ?>
                                    @endif

                                    @if ($item%2==0)
                                                <div class="col-12 col-md-4 m-3">
                                                    <div class="placeCard tilt cursor-pointer" style="background-image: url('{{ Storage::url($Place->images()->first()->url) }}');" data-id="{{ $Place->id }}" data-bs-toggle="modal" data-bs-target="#placeModal">
                                                        <h3 class="placeCard-title">{{ $Place->name }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if (count($Vacation->places) > $item)
                                        <div class="carousel-item">
                                            <div class="row justify-content-center">
                                        @endif
                                    @else
                                        @if (count($Vacation->places) == $item)
                                                <div class="col-12 col-md-4 m-3">
                                                    <div class="placeCard tilt cursor-pointer" style="background-image: url('{{ Storage::url($Place->images()->first()->url) }}');" data-id="{{ $Place->id }}" data-bs-toggle="modal" data-bs-target="#placeModal">
                                                        <h3 class="placeCard-title">{{ $Place->name }}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                                <div class="col-12 col-md-4 m-3">
                                                    <div class="placeCard tilt cursor-pointer" style="background-image: url('{{ Storage::url($Place->images()->first()->url) }}');" data-id="{{ $Place->id }}" data-bs-toggle="modal" data-bs-target="#placeModal">
                                                        <h3 class="placeCard-title">{{ $Place->name }}</h3>
                                                    </div>
                                                </div>
                                        @endif
                                    @endif
                                    <?php
                                        $item++;
                                    ?>
                                    
                                @endforeach

                                <button class="carousel-control-prev" type="button" data-bs-target="#uticelokKorhinta" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#uticelokKorhinta" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    @else
                        <p class="fw-bold text-center">
                            Még nincsenek uticélok!
                        </p>
                    @endif
                </div>
                <div class="col-12 shadow-lg rounded p-3">
                    <h5 class="fw-bold">Hozzászólások</h5>
                    @if (count($Vacation->comments) > 0)
                    @else
                        <p class="fw-bold text-center">
                            Még nincsenek hozzászólások!
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="newParticipantModal" tabindex="-1" aria-labelledby="newParticipantModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newParticipantModalLabel">Új résztvevő meghívása</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <p class="text-muted">
                        Kattintson az alábbi linkre a vágólapra másoláshoz, utána bárkivel bárhol megoszthatja.
                    </p>

                    <p class="text-dark text-decoration-underline cursor-pointer text-center copyOnClick" id="inviteLink">
                        {{ url('invite/'.$Vacation->nick) }}
                    </p>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Bezárás</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-xl" id="newPlaceModal" tabindex="-1" aria-labelledby="newPlaceModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newPlaceModalLabel">Új úti cél hozzáadása</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    
                    <form action="{{ url('nyaralasok/'.$Vacation->nick.'/uticelok/uj/mentes') }}" method="post" id="ujUticelForm" enctype="multipart/form-data">
                        <div class="mb-3">
                            <label for="uticelMegnevezese" class="form-label">Úti cél megnevezése</label>
                            <input type="text" class="form-control" id="uticelMegnevezese" name="uticelMegnevezese" placeholder="pl.: Texas" required>
                        </div>
                        <div class="mb-3">
                            <label for="uticelCim" class="form-label">Cím a szálláshoz</label>
                            <input type="text" class="form-control" id="uticelCim" name="uticelCim" placeholder="0000. Minta, Géza utca 5." required>
                        </div>
                        <div class="mb-3">
                            <label for="uticelLink" class="form-label">Link a szálláshoz</label>
                            <input type="text" class="form-control" id="uticelLink" name="uticelLink" placeholder="https://..." required>
                        </div>
                        <div class="mb-3">
                            <label for="szemelyenkentiSzallasAr" class="form-label">Személyenkénti szállás ár</label>
                            <div class="input-group">
                                <input type="number" class="form-control text-end" id="szemelyenkentiSzallasAr" name="szemelyenkentiSzallasAr" placeholder="0" required>
                                <span class="input-group-text" id="basic-addon2">,- Forint / fő</span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="szemelyenkentiUtazasiKoltseg" class="form-label">Személyenkénti utazási költség</label>
                            <div class="input-group">
                                <input type="number" class="form-control text-end" id="szemelyenkentiUtazasiKoltseg" name="szemelyenkentiUtazasiKoltseg" placeholder="0" required>
                                <span class="input-group-text" id="basic-addon2">,- Forint / fő</span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="uticelKepek" class="form-label">Képek feltöltése</label>
                            <input type="file" class="form-control" id="uticelKepek" name="uticelKepek[]" multiple required>
                        </div>
                        <div class="mb-3">
                            <label for="uticelLeiras" class="form-label">Rövid leírás az úti célhoz</label>
                            <textarea name="uticelLeiras" id="uticelLeiras"></textarea>
                        </div>
                        @csrf
                    </form>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Bezárás</button>
                    <button type="button" class="btn btn-primary" id="saveUjUticelForm">Mentés</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-lg h-100" id="placeModal" tabindex="-1" aria-labelledby="placeModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="placeModalLabel">Úti cél: <span></span></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="placeImagescarousel" class="carousel carousel-dark slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                    
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#placeImagescarousel" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#placeImagescarousel" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row h5">
                        <div class="col-12 h5">
                            <p>
                                <span class="fw-bold">
                                    Úti cél neve:
                                </span>
                                <span id="uticelNevSpan" class="fw-light"></span>
                            </p>
                        </div>
                        <div class="col-12 h5">
                            <p>
                                <span class="fw-bold">
                                    Úti cél címe:
                                </span>
                                <span id="uticelCimSpan" class="fw-light"></span>
                            </p>
                        </div>
                        <div class="col-12 h5">
                            <p>
                                <span class="fw-bold">
                                    Úti cél link:
                                </span>
                                <span id="uticelLinkSpan" class="fw-light"></span>
                            </p>
                        </div>
                        <div class="col-12 h5 lh-1">
                            <p>
                                <span class="fw-bold">
                                    Szállás költség:
                                </span>
                                <span id="uticelSzallasArSpan" class="fw-light"></span> Ft/fő<br>
                                <span class="text-muted fst-italic h6">
                                    Az itt megadott összeg a jelenlegi időtartamra szól: {{ $Vacation->from }} - {{ $Vacation->to }}
                                </span>
                            </p>
                        </div>
                        <div class="col-12 h5 lh-1">
                            <p>
                                <span class="fw-bold">
                                    Utazási költség:
                                </span>
                                <span id="uticelUtazasiKoltsegSpan" class="fw-light"></span> Ft/fő<br>
                                <span class="text-muted fst-italic h6">
                                    Az itt megadott összeg résztvevőnként eltérő lehet, pontos számításokat érdemes minden résztvevőnek saját magának kiszámolnia.
                                </span>
                            </p>
                        </div>
                        <div class="col-12">
                            <p class="h5">
                                <span class="fw-bold">
                                    Úti cél leírás:
                                </span>
                            </p>
                            <span id="uticelLeirasSpan"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Bezárás</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#szerepkorValaszto").on("change",function(){
            $("#szerepkorValasztoLeiras").html("")
            $("#szerepkorValasztoLeiras").html($(this).find(":selected").attr("data-description"))
        })

        $(".copyOnClick").on("click", function(){
            
            navigator.clipboard.writeText($(this).text());

            iziToast.success({
                title: "Siker!",
                message: "A szöveg sikeresen a vágólapra másolva!",
                position: "topRight"
            })
        })

        $("#saveUjUticelForm").on("click",function(){
            $("#ujUticelForm").trigger("submit")
        })

        $("#newPlaceModal").on("hidden.bs.modal",function(){
            $("#ujUticelForm").trigger("reset")
        })

        $("#placeModal").on("show.bs.modal",async function(e){
            let placeId = $(e.relatedTarget).attr("data-id");
            let keres = await fetch("{{ url('api/getPlaceInformations') }}",{
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "X-CSRF-Token": "{{ csrf_token() }}"
                },
                body: JSON.stringify({
                    placeId: placeId,
                })
            })

            let valasz = await keres.json();

            console
            
            $("#placeModalLabel span").text(valasz.data.name);
            generateCarousel(valasz.meta.images);
            $("#uticelNevSpan").text(valasz.data.name);
            $("#uticelCimSpan").text(valasz.data.address);
            if(valasz.data.link){
                $("#uticelLinkSpan").html("<a href='"+valasz.data.link+"' target='_blank'>"+valasz.data.link+"</a>");
            }else{
                $("#uticelLinkSpan").text("Nem adtak meg linket a szálláshoz.");
            }
            $("#uticelSzallasArSpan").text(valasz.data.accomodation_price_person);
            $("#uticelUtazasiKoltsegSpan").text(valasz.data.travel_cost_person);
            $("#uticelLeirasSpan").html(valasz.data.description);
        })

        function generateCarousel(data){
            $("#placeImagescarousel .carousel-inner").empty()
            var elso = true;
            for (const image of data) {
                if(elso){
                    elso = false;
                    $("#placeImagescarousel .carousel-inner").append(
                        $("<div></div>")
                            .addClass("carousel-item active")
                            .append(
                                $("<a></a>")
                                    .attr("href", "/storage/"+image.url)
                                    .attr("target", "_blank")
                                    .attr("rel", "noopener noreferrer")
                                    .append(
                                        $("<div></div>")
                                            .addClass("carousel__image")
                                            .attr("style","background-image: url(/storage/"+image.url+")")
                                    )
                            )
                    )
                }else{
                    $("#placeImagescarousel .carousel-inner").append(
                        $("<div></div>")
                            .addClass("carousel-item")
                            .append(
                                $("<a></a>")
                                    .attr("href", "/storage/"+image.url)
                                    .attr("target", "_blank")
                                    .attr("rel", "noopener noreferrer")
                                    .append(
                                        $("<div></div>")
                                            .addClass("carousel__image")
                                            .attr("style","background-image: url(/storage/"+image.url+")")
                                    )
                            )
                    )
                }
            }
        }

    </script>
@endsection