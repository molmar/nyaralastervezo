@extends('layouts.app')

@section('content')
    <div class="row mb-3">
        <a class="text-decoration-none text-reset" data-bs-toggle="collapse" href="#inProgress" role="button" aria-expanded="false" aria-controls="inProgress">
            <h3>
                Tervezés alatt
            </h3>
            <hr>
        </a>
        <div class="collapse show" id="inProgress">
            <div class="container-fluid">
                <div class="row justify-content-around">
                    @if (count($OUVacations) > 0)
                        @foreach ($OUVacations as $OUVacation)
                            @if ($OUVacation->status->under_organization == 1)
                                <div class="col-12 col-md-3 col-lg-2 m-2">
                                    <div class="vacationCard tilt" style="background-image: url('{{ $OUVacation->thumbnail->url }}');" data-nick="{{ $OUVacation->nick }}">
                                        <h3 class="vacationCard-title">{{ $OUVacation->name }}</h3>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="col-12 text-center">
                            Még nincsenek tervezett nyaralások
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <a class="text-decoration-none text-reset" data-bs-toggle="collapse" href="#oldVacations" role="button" aria-expanded="false" aria-controls="oldVacations">
            <h3>
                Régebbi nyaralások
            </h3>
            <hr>
        </a>
        <div class="collapse show" id="oldVacations">
            <div class="container-fluid">
                <div class="row justify-content-around">
                    @if (count($OVacations) > 0)
                        @foreach ($OVacations as $OVacation)
                            @if ($OVacation->status->retained == 1)
                                <div class="col-12 col-md-3 col-lg-2 m-2">
                                    <div class="vacationCard tilt" style="background-image: url('{{ $OVacation->thumbnail->url }}');" data-nick="{{ $OVacation->nick }}">
                                        <h3 class="vacationCard-title">{{ $OVacation->name }}</h3>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="col-12 text-center">
                            Még nem voltak nyaralások
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <a class="text-decoration-none text-reset" data-bs-toggle="collapse" href="#canceled" role="button" aria-expanded="false" aria-controls="canceled">
            <h3>
                Elmaradt nyaralások
            </h3>
            <hr>
        </a>
        <div class="collapse show" id="canceled">
            <div class="container-fluid">
                <div class="row justify-content-around">
                    @if (count($CVacations) > 0)
                        @foreach ($CVacations as $CVacation)
                            @if ($CVacation->status->cancaled == 1)
                                <div class="col-12 col-md-3 col-lg-2 m-2">
                                    <div class="vacationCard tilt" style="background-image: url('{{ $CVacation->thumbnail->url }}');" data-nick="{{ $CVacation->nick }}">
                                        <h3 class="vacationCard-title">{{ $CVacation->name }}</h3>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="col-12 text-center">
                            Még nem maradtak el nyaralások
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection