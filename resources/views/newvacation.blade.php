@extends('layouts.app')

@section('content')
    <h3 class="fw-bold">
        Új nyaralás létrehozása
    </h3>
    <div class="mx-0 mx-lg-5 mb-5">
        <form action="{{ url('nyaralasok/uj') }}" method="post" class="mt-4" id="ujVakacioForm">
            <h4 class="mt-4">
                Alapadatok
            </h4>
            <hr>
            <div class="mb-3">
                <label for="vakacioNeve" class="form-label">Vakáció neve</label>
                <input type="text" class="form-control" id="vakacioNeve" name="vakacioNeve" placeholder="Vakáció neve">
            </div>
            <div class="mb-3">
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" role="switch" id="idopontKivalasztva" name="idopontKivalasztva">
                    <label class="form-check-label" for="idopontKivalasztva">Az időpont már kiválasztásra került</label>
                </div>
            </div>
            <div class="d-none" id="date_setter">
                <div class="mb-3">
                    <label for="vakacioKezdes" class="form-label">Vakáció kezdő dátuma</label>
                    <input type="date" class="form-control" id="vakacioKezdes" name="vakacioKezdes">
                </div>
                <div class="mb-3">
                    <label for="vakacioVege" class="form-label">Vakáció záró dátuma</label>
                    <input type="date" class="form-control" id="vakacioVege" name="vakacioVege">
                </div>
                <div class="mb-3">
                    <label for="vakacioIdopontszervezoLink" class="form-label">Időpontszervező URL <span class="text-muted fst-italic">(Pl.: Doodle link) ()</span></label>
                    <input type="text" class="form-control" id="vakacioIdopontszervezoLink" name="vakacioIdopontszervezoLink" placeholder="https://">
                </div>
            </div>
            <h4 class="mt-4">
                Indexkép
            </h4>
            <hr>
            <p>
                A lenti kártyákra kattintva válaszd ki a nyaralás indexképét.
            </p>
            <input type="hidden" name="vakacioIndexkepId" id="vakacioIndexkepId">
            <div class="container-fluid">
                <div class="row justify-content-around">
                    @if (count($Thumbnails) > 0)
                        @foreach ($Thumbnails as $Thumbnail)
                            <div class="col-12 col-md-3 col-lg-2 m-2 p-0">
                                <div class="thumbnailCard tilt" style="background-image: url('{{ $Thumbnail->url }}');" data-id="{{ $Thumbnail->id }}" data-tilt>
                                    <h3 class="thumbnailCardExampleText"></h3>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-12 text-center">
                            Nincsenek feltöltött indexképek az oldalon
                        </div>
                    @endif
                </div>
            </div>
            <h4 class="mt-4">
                Műveletek
            </h4>
            <hr>
            <div class="mt-3">
                <input type="submit" value="Létrehozás" class="btn btn-primary me-2">
                <input type="reset" value="Mégsem" class="btn btn-outline-secondary ms-2">
            </div>
            @csrf
        </form>
    </div>
@endsection

@section('scripts')

    <script>
        $("#idopontKivalasztva").on("change",function(){
            $("#date_setter").toggleClass("d-none d-block");
        })

        $("#vakacioNeve").on("input",setThumbnailExampleText).on("change",setThumbnailExampleText)

        function setThumbnailExampleText(){
            $(".thumbnailCardExampleText").text($("#vakacioNeve").val())
        }

        $("#ujVakacioForm").on("reset",function(){
            $(".thumbnailCardExampleText").text(null)
            $(".thumbnailCard").removeClass("selected")
            $("#date_setter").removeClass("d-block");
            $("#date_setter").addClass("d-none");
            $("#vakacioIndexkepId").val(none);
        })

        $("#ujVakacioForm").on("submit",function(e){
            var error = false

            if($("#vakacioIndexkepId").val()==""){
                iziToast.error({
                    title:"Hiba!",
                    message:"Válasszon ki egy indexképet!",
                    position: "topRight",
                })

                error = true
            }

            if($("#idopontKivalasztva").is(":checked")){
                if($("#vakacioKezdes").val()=="" && $("#vakacioVege").val()==""){
                    iziToast.error({
                        title:"Hiba!",
                        message:"A vakáció dátumának beállítása kötelező!",
                        position: "topRight",
                    })

                    error = true
                }
            }

            if($("#vakacioNeve").val()==""){
                iziToast.error({
                    title:"Hiba!",
                    message:"Név megadása kötelező!",
                    position: "topRight",
                })

                error = true
            }

            if(error){
                e.preventDefault()
            }
        })
    </script>
    
@endsection