<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ config('app.name') }}</title>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>
    <body>


        <div class="container vh-100 d-flex align-items-center">
            <div class="card mx-auto">
                <h5 class="card-header">
                    Bejelentkezés
                </h5>
                <div class="card-body">
                    <h5 class="card-title">Az oldal használatához bejelentkezés szükséges!</h5>
                    <p class="card-text">Amennyiben még nem jártál az oldalon, az első bejelentkezés alkalmával létrehozzuk felhasználói fiókodat is!</p>
                    <a href="{{ url('auth/google') }}" class="btn btn-outline-primary w-100 fw-semibold">Bejelentkezés Google fiókkal</a>
                </div>
                <div class="card-footer text-muted text-center">
                    Alkalmazás verzió: {{ config('app.version') }}
                </div>
            </div>
        </div>
        

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>