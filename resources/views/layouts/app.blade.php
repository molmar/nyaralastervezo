<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ config('app.name') }}</title>

        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
        <script src="https://kit.fontawesome.com/bdb7eb035d.js" crossorigin="anonymous"></script>
        <script src="https://cdn.tiny.cloud/1/6z7as78k7pwnm1evnw25ri48liws5rq0kfo83vdfcx7ajjh7/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    </head>
    <body>


        <nav class="navbar navbar-expand-lg navbar-dark sticky-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="{{ url('attekintes') }}">{{ config('app.name') }}</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="navbar-nav mx-auto mb-2 mb-lg-0 text-center">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="{{ url('attekintes') }}">Áttekintés</a>
                        </li>
                        <li class="nav-item dropdown">
                            
                            <a class="nav-link dropdown-toggle" href="{{ url('nyaralasok') }}" id="nyaralasokDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Nyaralások
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark text-center text-lg-start" aria-labelledby="nyaralasokDropdown">
                                <li><a class="dropdown-item" href="{{ url('nyaralasok/uj') }}">Új nyaralás létrehozása</a></li>
                                <li><a class="dropdown-item" href="#">Általam létrehozottak</a></li>
                                <li><a class="dropdown-item" href="#">Velem megosztottak</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown d-lg-none">
                            
                            <a class="nav-link dropdown-toggle text-light text-decoration-underline" href="#" id="userDropdownNav" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                {{ $LoggedUser->name }}
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark text-center" aria-labelledby="userDropdownNav">
                                <li><a class="dropdown-item" href="{{ route('logout') }}">Kijelentkezés</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                
                <div class="ms-auto dropdown text-light d-none d-lg-block">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        {{ $LoggedUser->name }}
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark dropdown-menu-end" aria-labelledby="userDropdown">
                        <li><a class="dropdown-item" href="{{ route('logout') }}">Kijelentkezés</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container mt-5">
            @yield('content')
        </div>

        @yield('modals')
        
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/gh/mgalante/jquery.redirect@master/jquery.redirect.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js"></script>

        <script src="{{ asset('js/app.js') }}"></script>

        @yield('scripts')

        @if ($errors->any())
            <script>
                @foreach ($errors->all() as $error)
                    iziToast.error({
                        title: 'Hiba!',
                        message: '{{ $error }}',
                        position: "topRight",
                    });
                @endforeach
            </script>
        @endif

        @if(Session::get('success'))
            <script>
                iziToast.success({
                    title: 'Siker:',
                    message: '{{ session("success") }}',
                    position: 'topRight',
                });
            </script>
        @endif

        @if(Session::get('info'))
            <script>
                iziToast.info({
                    title: 'Info:',
                    message: '{{ session("info") }}',
                    position: 'topRight',
                });
            </script>
        @endif
        
        @if(Session::get('error'))
            <script>
                iziToast.error({
                    title: 'Hiba:',
                    message: '{{ session("error") }}',
                    position: 'topRight',
                });
            </script>
        @endif
    </body>
</html>