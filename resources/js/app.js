require('./bootstrap');

const bootstrap = require('bootstrap');

const anime = require('animejs');
const { default: iziToast } = require('izitoast');

/* $('.tilt').on('mouseenter', function () {
    animate = false;
}).on('mousemove', mouseMove).on('mouseleave', mouseLeave);
  
function mouseMove(e) {
    var bRect = this.getBoundingClientRect();
    var mX = e.pageX;
    var mY = e.pageY;
    var pX = Math.floor((mX - bRect.left) / bRect.width * 100);
    var pY = Math.floor((mY - bRect.top) / bRect.height * 100);
    var rotX = (pY - 50) / 5;
    var rotY = -(pX - 50) / 5;
    var shd = '0 ' + rotX + 'px ' + Math.abs(rotX) * 5 + 'px rgba(0,0,50,.15), 0 ' + pY / 10 + 'px ' + pY / 5 + 'px rgba(0,0,0,.25)';
    var trs = 'rotateX(' + rotX + 'deg) rotateY(' + rotY + 'deg)';

    $(this).css('box-shadow', shd);
    $(this).css('transform', trs);
}

function mouseLeave() {
    $(this).css('box-shadow', '');
    $(this).css('transform', '');
} */

$(".tilt").tilt({
    glare: true,
    maxGlare: .5,
})

$(document).ready(function(){

    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
    
    $("#navbar a").each(function(i,e){
        var menuHref = $(e).attr("href").split('/')
        var curLoc = window.location.pathname.slice(1).split('/')[0]

        if(menuHref !== undefined){
            if(menuHref[3] ==curLoc){
                if(!$(e).hasClass("dropdown-item"))
                    $(e).addClass("active")
            }
        }else if(menuHref[0] != "#" && curLoc==""){
            $(e).addClass("active")
        }
    });

    $(".vacationCard").on("click",function(){
        window.location.href = window.location.origin+"/nyaralasok/"+$(this).attr("data-nick")
    })

    $(".thumbnailCard").on("click",function(){
        if($(this).hasClass("selected")){
            $(this).removeClass("selected")
            $("#vakacioIndexkepId").val(null)
        }else{
            $(".thumbnailCard").removeClass("selected")
            $(this).addClass("selected")
            $("#vakacioIndexkepId").val($(this).attr("data-id"))
        }
    })

    tinymce.init({
        selector: 'textarea',
    });
})
