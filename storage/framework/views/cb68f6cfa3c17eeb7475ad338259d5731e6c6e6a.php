<?php $__env->startSection('content'); ?>
    <div class="row mb-3">
        <a class="text-decoration-none text-reset" data-bs-toggle="collapse" href="#inProgress" role="button" aria-expanded="false" aria-controls="inProgress">
            <h3>
                Tervezés alatt
            </h3>
            <hr>
        </a>
        <div class="collapse show" id="inProgress">
            <div class="container-fluid">
                <div class="row justify-content-around">
                    <?php if(count($OUVacations) > 0): ?>
                        <?php $__currentLoopData = $OUVacations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $OUVacation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($OUVacation->status->under_organization == 1): ?>
                                <div class="col-12 col-md-3 col-lg-2 m-2">
                                    <div class="vacationCard tilt" style="background-image: url('<?php echo e($OUVacation->thumbnail->url); ?>');" data-nick="<?php echo e($OUVacation->nick); ?>">
                                        <h3 class="vacationCard-title"><?php echo e($OUVacation->name); ?></h3>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <div class="col-12 text-center">
                            Még nincsenek tervezett nyaralások
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <a class="text-decoration-none text-reset" data-bs-toggle="collapse" href="#oldVacations" role="button" aria-expanded="false" aria-controls="oldVacations">
            <h3>
                Régebbi nyaralások
            </h3>
            <hr>
        </a>
        <div class="collapse show" id="oldVacations">
            <div class="container-fluid">
                <div class="row justify-content-around">
                    <?php if(count($OVacations) > 0): ?>
                        <?php $__currentLoopData = $OVacations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $OVacation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($OVacation->status->retained == 1): ?>
                                <div class="col-12 col-md-3 col-lg-2 m-2">
                                    <div class="vacationCard tilt" style="background-image: url('<?php echo e($OVacation->thumbnail->url); ?>');" data-nick="<?php echo e($OVacation->nick); ?>">
                                        <h3 class="vacationCard-title"><?php echo e($OVacation->name); ?></h3>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <div class="col-12 text-center">
                            Még nem voltak nyaralások
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <a class="text-decoration-none text-reset" data-bs-toggle="collapse" href="#canceled" role="button" aria-expanded="false" aria-controls="canceled">
            <h3>
                Elmaradt nyaralások
            </h3>
            <hr>
        </a>
        <div class="collapse show" id="canceled">
            <div class="container-fluid">
                <div class="row justify-content-around">
                    <?php if(count($CVacations) > 0): ?>
                        <?php $__currentLoopData = $CVacations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $CVacation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($CVacation->status->cancaled == 1): ?>
                                <div class="col-12 col-md-3 col-lg-2 m-2">
                                    <div class="vacationCard tilt" style="background-image: url('<?php echo e($CVacation->thumbnail->url); ?>');" data-nick="<?php echo e($CVacation->nick); ?>">
                                        <h3 class="vacationCard-title"><?php echo e($CVacation->name); ?></h3>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <div class="col-12 text-center">
                            Még nem maradtak el nyaralások
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/molmar/nyaralas/resources/views/dashboard.blade.php ENDPATH**/ ?>