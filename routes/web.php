<?php

use App\Http\Controllers\GoogleSocialiteController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PlaceController;
use App\Http\Controllers\VacationController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()){
        return redirect('attekintes');
    }else{
        return redirect('bejelentkezes');
    }
});

Route::get('/bejelentkezes', [LoginController::class,'show'])->name('login');

Route::get('/kijelentkezes', function () {
    if(Auth::check()){
        Auth::logout();
    }
    return redirect('/');
})->name('logout');

Route::middleware(["redirectIfNotAuthenticated","shareLoggedUserInformationWithViews"])->group(function(){
    Route::get('attekintes',[HomeController::class,"dashboard"]);
    Route::prefix('nyaralasok')->group(function(){
        Route::get('/',function(){
            return redirect('attekintes');
        });
        Route::get("uj",[HomeController::class,"newVacation"]);
        Route::post("uj",[VacationController::class,"create"]);
        Route::get("{nick}",[VacationController::class,"show"]);
        Route::post("{nick}/uticelok/uj/mentes",[PlaceController::class,"saveNew"]);
    });
    Route::get('invite/{nick}',[VacationController::class,"invite"]);
});

Route::get('auth/google', [GoogleSocialiteController::class, 'redirectToGoogle']);
Route::get('callback/google', [GoogleSocialiteController::class, 'handleCallback']);
