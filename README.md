
# Nyaralástervező

2022-ben pár haverral elkezdtünk szervezkedni, hogy el kellene menni valahova közösen nyaralni. Elkezdtük megtervezni az egészet Google táblázatban, Doodle-vel kiválasztottuk a mindenkinek megfelelő időpontot, stb. Viszont végül ez a táblázat átláthatatlanná vált, illetve sosincs jó vége, ha 5-6 ember egyszerre szerkeszt egy doksit egymástól teljesen független időben. Erre szerettem volna egy megoldást találni, így kerestem több lehetőséget is az interneten, de valahogy semmi jót nem találtam, így eldöntöttem, hogy csinálok egy saját webalkalmazást.

A projekt elérhető a https://nyaralas.molmar.tk webcímen keresztül.
## Használt technológiák
Ebben az időben (2022 nyara) kezdtem el komolyabban foglalkozni a Laravel nevű PHP keretrendszerrel, így adott volt, hogy ez fogja adni az alapjait a weboldalnak. A front-end pedig a Bootstrap keretrendszer akkor éppen aktuális (5.2.0-beta1) verziójával készült. Mivel igyekeztem a lehető legegyszerűbbre megtervezni az alkalmazás használatát, így a Laravel Socialite moduljának segítségével építettem fel a bejelentkezést, így egyszerűen, a Google fiókunk segítségével egy kattintással már használhatjuk is az alkalmazást.
## Funkciók
A jelenleg is működő funkciók a következők:
- Új nyaralás létrehozása
    - Nyaralás nevének megadása
    - Nyaralás időpontjának beállítása *(opcionális)*
    - Indexkép kiválasztása 10 stock képből
- Úti célok megadása
    - Úti cél megnevezése
    - Cím a szálláshoz
    - Link a szálláshoz
    - Személyenkénti szállás ár
    - Személyenkénti utazási költség
    - Képek feltöltése
    - Rövid leírás az úti célhoz
- Személyek meghívása a nyaralásba egyedi linkkel
WIP funkciók:
- "Általam létrehozott" és "Velem megosztott" menüpontok
- Személyes beállítások
- Nyaralások beállításai
- Nyaraláshoz csatlakozott személyek kezelése
- Felhasználói jogosultsági szintek az egyes nyaralásoknál
- Hozzászólások panel
- A felületen keresztüli időpont kiválasztó rendszer
- *és ami még fejlesztés közben az eszembe jut, mert biztos, hogy lesz ilyen... :D*
## A projekt jövője

Sajnos a tervezett nyaralás végül elmaradt és ezáltal a weboldal sem lett teljesen befejezve, viszont továbbra is tevben van egy teljesen működő verzió elkészítése a *WIP funkciók* megvalósításával.
## Badges

[![Website](https://img.shields.io/website?down_color=red&down_message=down&label=nyaralas.molmar.tk&up_color=green&up_message=up&url=https%3A%2F%2Fnyaralas.molmar.tk)](https://nyaralas.molmar.tk)
